defmodule Takso.Auth.Pipeline do
  use Guardian.Plug.Pipeline,
    otp_app: :Takso,
    error_handler: Takso.Auth.ErrorHandler,
    module: Takso.Auth.Guardian

  plug(Guardian.Plug.VerifySession, claims: %{"typ" => "access"})
  plug(Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}, realm: "Bearer")
  plug(Guardian.Plug.LoadResource, allow_blank: true)
end
