defmodule Takso.Auth.Auth do
  import Ecto.Query, only: [from: 2]
  alias Comeonin.Bcrypt
  alias Takso.{Repo, User}

  def authenticate_user(email, plain_text_password) do
    query = from(u in User, where: u.email == ^email, select: u)

    case Repo.one(query) do
      nil ->
        Bcrypt.dummy_checkpw()
        {:error, :invalid_credentials}
      user -> check_password(user, plain_text_password)
    end
  end

  defp check_password(user, plain_text_password) do
    case Bcrypt.checkpw(plain_text_password, user.encrypted_password) do
      true -> {:ok, user}
      false -> {:error, :invalid_credentials}
    end
  end
end
