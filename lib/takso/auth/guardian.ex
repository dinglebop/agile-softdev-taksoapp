defmodule Takso.Auth.Guardian do
  use Guardian, otp_app: :takso

  def subject_for_token(user, _claims) do
    {:ok, to_string(user.id)}
  end

  def resource_from_claims(%{"sub" => id}) do
    case Takso.Repo.get!(Takso.User, id) do
      nil -> {:error, :resource_not_found}
      user -> {:ok, user}
    end
  end

  def resource_from_claims(_claims) do
    {:error, :no_sub_provided}
  end
end
