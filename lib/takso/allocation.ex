defmodule Takso.Allocation do
  use Ecto.Schema
  import Ecto.Changeset

  schema "allocations" do
    field(:status, :string)
    field(:rating, :float)
    belongs_to(:taxi, Takso.Taxi, foreign_key: :taxi_id)
    belongs_to(:booking, Takso.Booking, foreign_key: :booking_id)

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:status, :rating])
    |> validate_required([:status])
  end
end
