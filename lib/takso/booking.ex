defmodule Takso.Booking do
  use Ecto.Schema
  import Ecto.Changeset

  schema "bookings" do
    field(:pickup_address, :string)
    field(:dropoff_address, :string)
    field(:status, :string)
    field(:transport_start, :utc_datetime)
    field(:transport_end, :utc_datetime)
    belongs_to(:user, Takso.User)

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:pickup_address, :dropoff_address, :status, :transport_start, :transport_end])
    |> validate_required([:pickup_address, :dropoff_address])
  end
end
