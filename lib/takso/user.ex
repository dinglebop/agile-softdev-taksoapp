defmodule Takso.User do
  use Ecto.Schema
  import Ecto.Changeset
  import Comeonin.Bcrypt, only: [hashpwsalt: 1]

  schema "users" do
    field(:name, :string)
    field(:email, :string)
    field(:encrypted_password, :string)
    field(:role, :string)
    has_many(:bookings, Takso.Booking)

    # Virtual fields
    field(:password, :string, virtual: true)

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :email, :password, :role])
    |> validate_required([:name, :email, :password, :role])
    |> validate_format(:email, ~r/^[A-Za-z0-9._%+-+']+@[A-Za-z0-9.-]+\.[A-Za-z]+$/)
    |> validate_length(:password, min: 8)
    |> unique_constraint(:email)
    |> encrypt_password
  end

  def encrypt_password(changeset) do
    if changeset.valid? do
      put_change(
        changeset,
        :encrypted_password,
        hashpwsalt(changeset.changes[:password])
      )
    else
      changeset
    end
  end
end
