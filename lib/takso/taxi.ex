defmodule Takso.Taxi do
  use Ecto.Schema
  import Ecto.Changeset

  schema "taxis" do
    field(:email, :string)
    field(:location, :string)
    field(:status, :string)

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email, :location, :status])
    |> validate_required([:email, :location, :status])
  end
end
