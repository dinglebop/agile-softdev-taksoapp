defmodule Takso.Api.UserView do
  use TaksoWeb, :view

  def render("user.json", %{user: user}) do
    %{user: %{email: user.email, role: user.role}}
  end

  def render("user_auth.json", %{user: user, token: token}) do
    %{user: %{email: user.email, role: user.role}, jwt: token}
  end
end
