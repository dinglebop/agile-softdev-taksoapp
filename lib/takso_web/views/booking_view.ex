defmodule Takso.Api.BookingView do
  use TaksoWeb, :view

  def render("history.json", %{history: history}) do
    %{history: Enum.map(history, &booking_json/1)}
  end

  def booking_json(booking) do
    %{
      created: booking.inserted_at,
      pickup_address: booking.pickup_address,
      dropoff_address: booking.dropoff_address,
      status: booking.status,
      transport_start: booking.transport_start,
      transport_end: booking.transport_end
    }
  end
end
