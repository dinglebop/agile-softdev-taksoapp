defmodule Takso.TaxiAllocator do
  use GenServer
  import Ecto.Query, only: [from: 2]

  alias Ecto.{Changeset, Multi}
  alias Takso.{Repo, Taxi, Booking, Allocation}

  def start_link(booking, booking_reference) do
    GenServer.start_link(__MODULE__, booking, name: booking_reference)
  end

  def init(booking) do
    case provision_taxi(booking) do
      {:noreply, state} -> {:ok, state}
      {:stop, :normal, booking} -> {:stop, :normal, booking}
    end
  end

  def driver_response(pid, response) do
    GenServer.cast(pid, response)
  end

  def handle_cast(:accepted, {booking, allocation, _}) do
    taxi_query = from(t in Taxi, where: t.id == ^allocation.taxi_id, select: t)
    taxi = Repo.one(taxi_query)

    Multi.new()
    |> Multi.update(:allocation, Allocation.changeset(allocation, %{status: "accepted"}))
    |> Multi.update(:taxi, Taxi.changeset(taxi) |> Changeset.put_change(:status, "busy"))
    |> Multi.update(
      :booking,
      Booking.changeset(booking) |> Changeset.put_change(:status, "accepted")
    )
    |> Repo.transaction()

    TaksoWeb.Endpoint.broadcast!("customer:" <> booking.user.email, "requests", %{
      msg: "Your taxi is on the way, you will be notified when it arrives",
      status: "accepted",
      booking_id: booking.id
    })

    {:noreply, {booking, allocation, []}}
  end

  def handle_cast(:rejected, {booking, allocation, taxis}) do
    Allocation.changeset(allocation, %{status: "rejected"})
    |> Repo.update!()

    provision_taxi(booking, taxis ++ [allocation.taxi_id])
  end

  def provision_taxi(booking, taxis \\ []) do
    IO.puts("Provisioning taxi")
    user = booking.user

    query = from(t in Taxi, where: t.status == "available" and t.id not in ^taxis, select: t)
    available_taxis = Repo.all(query)

    if length(available_taxis) > 0 do
      IO.puts("There are available taxis :)")

      # Take first taxi
      taxi = List.first(available_taxis)

      # Create a new allocation and insert it
      allocation_changeset =
        Allocation.changeset(%Allocation{}, %{status: "preallocated"})
        |> Changeset.put_change(:booking_id, booking.id)
        |> Changeset.put_change(:taxi_id, taxi.id)

      allocation = Repo.insert!(allocation_changeset)

      # Broadcast to taxi driver the request
      TaksoWeb.Endpoint.broadcast("driver:" <> taxi.email, "requests", %{
        id: booking.id,
        pickup_address: booking.pickup_address,
        dropoff_address: booking.dropoff_address
      })

      {:noreply, {booking, allocation, taxis}}
    else
      IO.puts("No available taxis")

      Booking.changeset(booking)
      |> Changeset.put_change(:status, "rejected")
      |> Repo.update()

      TaksoWeb.Endpoint.broadcast("customer:" <> user.email, "requests", %{
        msg: "Our apologies, we cannot serve your request at this moment",
        status: "rejected"
      })

      {:stop, :normal, booking}
    end
  end
end
