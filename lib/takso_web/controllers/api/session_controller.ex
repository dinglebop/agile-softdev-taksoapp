defmodule Takso.Api.SessionController do
  use TaksoWeb, :controller
  alias Takso.Auth.{Auth, Guardian}

  plug :put_view, Takso.Api.UserView

  action_fallback Takso.Api.FallbackController

  def new(conn, %{"email" => email, "password" => password}) do
    with {:ok, user} <- Auth.authenticate_user(email, password),
         {:ok, token, _claims} <- Guardian.encode_and_sign(user) do
      conn
      |> put_status(201)
      |> render("user_auth.json", %{user: user, token: token})
    else
      {:error, reason} -> {:error, reason}
    end
  end

  def delete(conn, _params) do
    conn
    |> Guardian.Plug.current_token()
    |> Guardian.revoke()

    conn
    |> put_status(200)
    |> json(%{msg: "Logged out"})
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_status(403)
    |> json(%{msg: "You are not logged in"})
  end
end
