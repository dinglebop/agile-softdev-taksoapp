defmodule Takso.Api.UserController do
  use TaksoWeb, :controller
  alias Takso.{Repo, User, Taxi, Auth.Guardian}

  action_fallback Takso.Api.FallbackController

  def create(conn, %{"user" => user_params = %{"role" => "taxi-driver"}}) do
    changeset = User.changeset(%User{}, user_params)

    with {:ok, user} <- Repo.insert(changeset),
         {:ok, _} <-
           Repo.insert(
             Taxi.changeset(%Taxi{}, %{
               email: user.email,
               location: user_params["taxi_location"],
               status: "available"
             })
           ) do
      conn
      |> put_status(201)
      |> render("user.json", %{user: user})
    else
      {:error, changeset} -> {:error, changeset}
    end
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.changeset(%User{}, user_params)

    case Repo.insert(changeset) do
      {:ok, user} ->
        conn
        |> put_status(201)
        |> render("user.json", %{user: user})

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def show(conn, _params) do
    user = Guardian.Plug.current_resource(conn)

    conn
    |> put_status(200)
    |> json(%{user: %{id: user.id, email: user.email, role: user.role}})
  end
end
