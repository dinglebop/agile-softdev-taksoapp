defmodule Takso.Api.BookingController do
  use TaksoWeb, :controller
  import Ecto.Query, only: [from: 2]

  alias Takso.{Repo, Booking, Taxi, User, Allocation}
  alias Ecto.Changeset

  def create(conn, booking_params) do
    user = Guardian.Plug.current_resource(conn)

    struct =
      Ecto.build_assoc(
        user,
        :bookings,
        Enum.map(booking_params, fn {key, value} -> {String.to_atom(key), value} end)
      )

    changeset =
      Booking.changeset(struct)
      |> Changeset.put_change(:status, "open")

    booking =
      Repo.insert!(changeset)
      |> Repo.preload(:user)

    count_query = from(t in Taxi, where: t.status == "available", select: count(t))

    available_taxis = Repo.one(count_query)
    IO.inspect(available_taxis, label: "Available taxis")

    if available_taxis > 0 do
      Takso.TaxiAllocator.start_link(booking, String.to_atom("booking_#{booking.id}"))

      put_status(conn, 201)
      |> json(%{msg: "We are processing your request", status: "processing"})
    else
      Booking.changeset(booking)
      |> Changeset.put_change(:status, "rejected")
      |> Repo.update()

      put_status(conn, 409)
      |> json(%{
        msg: "Our apologies, we cannot serve your request at this moment",
        status: "rejected"
      })
    end
  end

  def update(conn, params) do
    booking_id = params["id"]
    status = params["status"]
    pid = String.to_atom("booking_#{booking_id}")

    if Process.whereis(pid) != nil do
      Takso.TaxiAllocator.driver_response(pid, String.to_atom(status))

      conn
      |> put_status(200)
      |> json(%{msg: "Notification sent to the customer"})
    else
      conn
      |> put_status(404)
      |> json(%{msg: "Booking not found"})
    end
  end

  def driver_ready(conn, params) do
    query =
      from(u in User,
        join: b in Booking,
        on: u.id == b.user_id,
        where: b.id == ^params["id"],
        select: u
      )

    case Repo.one(query) do
      nil ->
        conn
        |> put_status(404)
        |> json(%{msg: "Booking not found"})

      user ->
        TaksoWeb.Endpoint.broadcast("customer:" <> user.email, "requests", %{
          msg: "Your taxi is now at the pick-up location",
          status: "arrived"
        })

        conn
        |> put_status(200)
        |> json(%{msg: "Customer notified"})
    end
  end

  def start_transport(conn, params) do
    query = from(b in Booking, where: b.id == ^params["id"], select: b, preload: [:user])

    case Repo.one(query) do
      nil ->
        conn
        |> put_status(404)
        |> json(%{msg: "Booking not found"})

      booking ->
        Booking.changeset(booking, %{transport_start: DateTime.utc_now()})
        |> Repo.update!()

        TaksoWeb.Endpoint.broadcast("customer:" <> booking.user.email, "requests", %{
          msg: "Your taxi ride is currently ongoing",
          status: "started"
        })

        conn
        |> put_status(200)
        |> json(%{msg: "Trip started!"})
    end
  end

  def end_transport(conn, params) do
    taxi_user = Guardian.Plug.current_resource(conn)

    query = from(b in Booking, where: b.id == ^params["id"], select: b, preload: [:user])

    case Repo.one(query) do
      nil ->
        conn
        |> put_status(404)
        |> json(%{msg: "Booking not found"})

      booking ->
        Booking.changeset(booking, %{transport_end: DateTime.utc_now(), status: "completed"})
        |> Repo.update!()

        TaksoWeb.Endpoint.broadcast("customer:" <> booking.user.email, "requests", %{
          msg: "Trip complete, thank you for using our service",
          status: "completed"
        })

        taxi_query = from(t in Taxi, where: t.email == ^taxi_user.email, select: t)

        Repo.one!(taxi_query)
        |> Taxi.changeset(%{status: "available"})
        |> Repo.update!()

        conn
        |> put_status(200)
        |> json(%{msg: "Trip completed!"})
    end
  end

  def history(conn, params) do
    user = Guardian.Plug.current_resource(conn)

    query =
      from(b in Booking,
        where: b.user_id == ^user.id,
        limit: 10,
        order_by: [desc: b.inserted_at],
        select: b
      )

    history = Repo.all(query)

    conn
    |> put_status(200)
    |> render("history.json", %{history: history})
  end

  def give_rating(conn, params) do
    user = Guardian.Plug.current_resource(conn)
    booking_id = params["id"]
    rating = params["rating"]

    query =
      from(a in Allocation,
        where: a.status == "accepted" and a.booking_id == ^booking_id,
        select: a
      )

    case Repo.one(query) do
      nil ->
        conn
        |> put_status(404)
        |> json(%{msg: "Booking not found"})

      allocation ->
        Allocation.changeset(allocation, %{rating: rating})
        |> Repo.update!()

        conn
        |> put_status(200)
        |> json(%{msg: "Submitted successfully"})
    end
  end
end
