defmodule Takso.Api.FallbackController do
  use TaksoWeb, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> render(TaksoWeb.ChangesetView, "error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(TaksoWeb.ErrorView, :"404")
  end

  def call(conn, {:error, :invalid_credentials}) do
    conn
    |> put_status(:unauthorized)
    |> json(%{error: "Invalid username or password"})
  end

  def call(conn, {:error, _type}) do
    conn
    |> put_status(500)
    |> json(%{error: "Something went wrong"})
  end
end
