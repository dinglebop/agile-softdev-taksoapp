defmodule TaksoWeb.CustomerChannel do
  use TaksoWeb, :channel

  def join("customer:" <> email, _params, socket) do
    {:ok, socket}
  end

  def join(_room, _, _socket) do
    {:error, :unauthorized}
  end
end
