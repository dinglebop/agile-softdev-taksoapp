defmodule TaksoWeb.DriverChannel do
  use TaksoWeb, :channel

  def join("driver:" <> _email, _params, socket) do
    {:ok, socket}
  end

  def join(_room, _, _socket) do
    {:error, :unauthorized}
  end
end
