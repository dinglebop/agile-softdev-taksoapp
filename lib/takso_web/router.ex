defmodule TaksoWeb.Router do
  use TaksoWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug Takso.Auth.Pipeline
  end

  pipeline :api_auth do
    plug Guardian.Plug.EnsureAuthenticated, handler: Takso.Api.SessionController
  end

  scope "/", TaksoWeb do
    # Use the default browser stack
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", Takso do
    pipe_through :api

    put "/users", Api.UserController, :create
    put "/sessions", Api.SessionController, :new
  end

  scope "/api", Takso do
    pipe_through [:api, :api_auth]

    delete "/sessions", Api.SessionController, :delete
    get "/users", Api.UserController, :show
    put "/bookings", Api.BookingController, :create
    patch "/bookings/:id", Api.BookingController, :update
    get "/bookings/ready/:id", Api.BookingController, :driver_ready
    patch "/bookings/start/:id", Api.BookingController, :start_transport
    patch "/bookings/end/:id", Api.BookingController, :end_transport
    get "/bookings/history", Api.BookingController, :history
    patch "/bookings/:id/rating", Api.BookingController, :give_rating
  end
end
