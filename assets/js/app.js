import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';

import auth from './authentication';

import App from 'js/components/app.vue';
import Register from 'js/components/register-page.vue';
import Login from 'js/components/login-page.vue';
import Booking from 'js/components/booking-page.vue';
import BookingHistory from 'js/components/booking-history-page.vue';
import Transporter from 'js/components/transporter-page.vue';

Vue.use(VueRouter)
Vue.use(Vuetify)

const routes = [
  { name: 'register', path: '/register', component: Register },
  { name: 'login', path: '/login', component: Login },
  { name: 'booking', path: '/booking', component: Booking, meta: { requiresAuth: true } },
  { name: 'history', path: '/history', component: BookingHistory, meta: { requiresAuth: true } },
  { name: 'transporter', path: '/transporter', component: Transporter, meta: { requiresAuth: true } },
  { name: 'wildcard', path: '*', redirect: '/login' }
]

const router = new VueRouter({
  routes
});

// TODO: There is a better way to do this (auth middleware) outlined in the practice materials
router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.requiresAuth) && !auth.isAuthenticated()) {
    next({ name: 'login' });
  } else if (to.name == 'booking' && auth.getRole() === 'taxi-driver') {  // This is a bit cunty way of doing it, replace if something better comes up
    next({ name: 'transporter' })
  } else {
    next();
  }
});

new Vue({
  el: '#takso-app',
  components: { App },
  router: router
});