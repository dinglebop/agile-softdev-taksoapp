import http from 'js/requests';
import { Socket } from "phoenix";

const authChangedCallbacks = [];

export default {
    user: {
        email: '',
        name: '',
        role: ''
    },
    socket: null,
    login(context, credentials, redirect) {
        console.log('Logging in');
        return http.put('/api/sessions', credentials)
            .then(response => {
                this.user.email = credentials.email;

                this.user.name = response.data.user.name;
                this.user.role = response.data.user.role;

                const token = response.data.jwt;
                if (!token) {
                    throw 'No token received';
                }

                window.localStorage.setItem(`token-${this.user.email}`, token);
                http.setToken(token);

                this.socket = new Socket("/socket", { params: { token: token } });
                this.socket.connect();

                this.notifyAuthChanged();

                if (redirect) {
                    context.$router.push(redirect);
                }
            })
            .catch(error => {
                console.error(error);
                throw {
                    status: error.response.status,
                    error: error.response.data.error
                }
            });
    },
    logout(context, options) {
        http.delete(`/api/sessions`, options)
            .then((response) => {
                window.localStorage.removeItem(`token-${this.user.email}`);
                http.setToken('');

                this.user = {
                    email: '',
                    name: '',
                    role: ''
                }

                this.socket = null;

                this.notifyAuthChanged();
                context.$router.push('login')
            })
            .catch(error => {
                console.error(error);
            });
    },
    getRole() {
        return this.user.role;
    },
    getToken() {
        if (!this.user.email) {
            return null;
        }

        return window.localStorage.getItem('token-' + this.user.email);
    },
    isAuthenticated() {
        const jwt = this.getToken();

        return !!jwt;
    },
    getChannel(prefix) {
        const token = this.getToken();
        if (token === null) {
            return null;
        }

        return this.socket.channel(`${prefix}${this.user.email}`, { guardian_token: token });
    },
    onAuthChanged(callback) {
        authChangedCallbacks.push(callback);
    },
    notifyAuthChanged() {
        authChangedCallbacks.forEach(cb => cb(this.isAuthenticated()));
    }
}
