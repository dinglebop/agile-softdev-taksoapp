import axios from 'axios';

export default {
    defaultOptions: {
        headers: {
            Authorization: '',
        },
    },
    getToken() {
        return this.defaultOptions.headers.Authorization;
    },
    setToken(token) {
        this.defaultOptions.headers.Authorization = token ? `Bearer ${token}` : ''
    },
    get(url, options = {}) {
        return axios.get(url, { ...this.defaultOptions, ...options });
    },
    post(url, data, options = {}) {
        return axios.post(url, data, { ...this.defaultOptions, ...options });
    },
    put(url, data, options = {}) {
        return axios.put(url, data, { ...this.defaultOptions, ...options });
    },
    patch(url, data, options = {}) {
        return axios.patch(url, data, { ...this.defaultOptions, ...options });
    },
    delete(url, options = {}) {
        return axios.delete(url, { ...this.defaultOptions, ...options });
    },
};
