defmodule TaksoWeb.BookingControllerTest do
  use TaksoWeb.ConnCase

  alias Takso.{Repo, User}

  setup %{conn: conn} do
    Repo.insert!(
      User.changeset(%User{}, %{
        name: "Marta Maasikas",
        email: "marta@maasikas.ee",
        password: "test1234",
        role: "customer"
      })
    )

    %{"jwt" => token, "user" => _} =
      conn
      |> put(session_path(conn, :new, %{email: "marta@maasikas.ee", password: "test1234"}))
      |> json_response(201)

    conn =
      conn
      |> put_req_header("authorization", "Bearer " <> token)

    {:ok, conn: conn}
  end

  test "create a booking", %{conn: conn} do
    data = %{
      pickup_address: "Liivi 2",
      dropoff_address: "Eeden"
    }

    response =
      conn
      |> put(booking_path(conn, :create, data))
      |> json_response(201)

    expected = %{"msg" => "We are processing your request", "status" => "processing"}
    assert expected == response
  end
end
