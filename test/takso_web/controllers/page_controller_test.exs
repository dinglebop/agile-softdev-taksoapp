defmodule TaksoWeb.PageControllerTest do
  use TaksoWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert String.contains?(html_response(conn, 200), "Hello Takso!")
  end
end
