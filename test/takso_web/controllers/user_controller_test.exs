defmodule TaksoWeb.UserControllerTest do
  use TaksoWeb.ConnCase

  alias Takso.{Repo, User}

  test "create new customer user", %{conn: conn} do
    data = %{
      user: %{
        name: "Marta Maasikas",
        email: "marta@asd.com",
        password: "test1234",
        role: "customer"
      }
    }

    response =
      conn
      |> put(user_path(conn, :create, data))
      |> json_response(201)

    expected = %{"user" => %{"email" => "marta@asd.com", "role" => "customer"}}

    assert expected == response
  end

  test "fail to create user without name", %{conn: conn} do
    data = %{
      user: %{
        email: "marta@asd.com",
        password: "test1234",
        role: "customer"
      }
    }

    response =
      conn
      |> put(user_path(conn, :create, data))
      |> json_response(422)

    expected = %{"errors" => %{"name" => ["can't be blank"]}}

    assert expected == response
  end

  test "fail to create user without email", %{conn: conn} do
    data = %{
      user: %{
        name: "Marta Maasikas",
        password: "test1234",
        role: "customer"
      }
    }

    response =
      conn
      |> put(user_path(conn, :create, data))
      |> json_response(422)

    expected = %{"errors" => %{"email" => ["can't be blank"]}}

    assert expected == response
  end

  test "fail to create user with non-email in email field", %{conn: conn} do
    data = %{
      user: %{
        name: "Marta Maasikas",
        email: "marta@maasikas",
        password: "test1234",
        role: "customer"
      }
    }

    response =
      conn
      |> put(user_path(conn, :create, data))
      |> json_response(422)

    expected = %{"errors" => %{"email" => ["has invalid format"]}}

    assert expected == response
  end

  test "fail to create user without password", %{conn: conn} do
    data = %{
      user: %{
        name: "Marta Maasikas",
        email: "marta@maasikas.ee",
        role: "customer"
      }
    }

    response =
      conn
      |> put(user_path(conn, :create, data))
      |> json_response(422)

    expected = %{"errors" => %{"password" => ["can't be blank"]}}

    assert expected == response
  end

  test "fail to create user with too short password", %{conn: conn} do
    data = %{
      user: %{
        name: "Marta Maasikas",
        email: "marta@maasikas.ee",
        password: "test",
        role: "customer"
      }
    }

    response =
      conn
      |> put(user_path(conn, :create, data))
      |> json_response(422)

    expected = %{"errors" => %{"password" => ["should be at least 8 character(s)"]}}

    assert expected == response
  end

  test "fail to create user without role", %{conn: conn} do
    data = %{
      user: %{
        name: "Marta Maasikas",
        email: "marta@maasikas.ee",
        password: "test1234"
      }
    }

    response =
      conn
      |> put(user_path(conn, :create, data))
      |> json_response(422)

    expected = %{"errors" => %{"role" => ["can't be blank"]}}

    assert expected == response
  end

  test "fail to create user with already existing email", %{conn: conn} do
    data = %{
      user: %{
        name: "Marta Maasikas",
        email: "marta@maasikas.ee",
        password: "test1234",
        role: "customer"
      }
    }

    Repo.insert!(User.changeset(%User{}, data.user))

    response =
      conn
      |> put(user_path(conn, :create, data))
      |> json_response(422)

    expected = %{"errors" => %{"email" => ["has already been taken"]}}

    assert expected == response
  end

  test "create new taxi driver user", %{conn: conn} do
    data = %{
      user: %{
        name: "Margus Murakas",
        email: "margus@asd.com",
        password: "test1234",
        role: "taxi-driver",
        taxi_location: "Kalamaja"
      }
    }

    response =
      conn
      |> put(user_path(conn, :create, data))
      |> json_response(201)

    expected = %{"user" => %{"email" => "margus@asd.com", "role" => "taxi-driver"}}

    assert expected == response
  end

  test "fail to create driver user without taxi location", %{conn: conn} do
    data = %{
      user: %{
        name: "Margus Murakas",
        email: "margus@asd.com",
        password: "test1234",
        role: "taxi-driver"
      }
    }

    response =
      conn
      |> put(user_path(conn, :create, data))
      |> json_response(422)

    expected = %{"errors" => %{"location" => ["can't be blank"]}}

    assert expected == response
  end
end
