defmodule TaksoWeb.SessionControllerTest do
  use TaksoWeb.ConnCase

  alias Takso.{Repo, User}

  test "log in successfully", %{conn: conn} do
    Repo.insert!(
      User.changeset(%User{}, %{
        name: "Marta Maasikas",
        email: "marta@maasikas.ee",
        password: "test1234",
        role: "customer"
      })
    )

    %{"jwt" => _, "user" => user} =
      conn
      |> put(session_path(conn, :new, %{email: "marta@maasikas.ee", password: "test1234"}))
      |> json_response(201)

    expected_user = %{"email" => "marta@maasikas.ee", "role" => "customer"}

    assert expected_user == user
  end

  test "fail to log in", %{conn: conn} do
    Repo.insert!(
      User.changeset(%User{}, %{
        name: "Marta Maasikas",
        email: "marta@maasikas.ee",
        password: "test1234",
        role: "customer"
      })
    )

    response =
      conn
      |> put(session_path(conn, :new, %{email: "marta@maasikas.ee", password: "test12345"}))
      |> json_response(401)

    expected = %{"error" => "Invalid username or password"}

    assert expected == response
  end

  test "delete session after logging in", %{conn: conn} do
    Repo.insert!(
      User.changeset(%User{}, %{
        name: "Marta Maasikas",
        email: "marta@maasikas.ee",
        password: "test1234",
        role: "customer"
      })
    )

    %{"jwt" => token, "user" => _} =
      conn
      |> put(session_path(conn, :new, %{email: "marta@maasikas.ee", password: "test1234"}))
      |> json_response(201)

    response =
      conn
      |> put_req_header("authorization", "Bearer " <> token)
      |> delete(session_path(conn, :delete, %{}))
      |> json_response(200)

    expected = %{"msg" => "Logged out"}
    assert expected == response
  end

  test "fail to access restricted endpoint when not logged in", %{conn: conn} do
    response =
      conn
      |> delete(session_path(conn, :delete, %{}))
      |> json_response(401)

    expected = %{"error" => "unauthenticated"}

    assert expected == response
  end
end
