defmodule Takso.Repo.Migrations.CreateUsersTaxisBookingsAllocations do
  use Ecto.Migration

  def change do

    create table(:users) do
      add :name, :string
      add :email, :string
      add :encrypted_password, :string
      add :role, :string

      timestamps()
    end

    create table(:bookings) do
      add :pickup_address, :string
      add :dropoff_address, :string
      add :status, :string
      add :user_id, references(:users)

      timestamps()
    end

    create table(:taxis) do
      add :email, :string
      add :location, :string
      add :status, :string

      timestamps()
    end

    create table(:allocations) do
      add :status, :string
      add :taxi_id, references(:taxis)
      add :booking_id, references(:bookings)

      timestamps()
    end

    create index(:allocations, [:taxi_id])
    create index(:allocations, [:booking_id])
  end
end
