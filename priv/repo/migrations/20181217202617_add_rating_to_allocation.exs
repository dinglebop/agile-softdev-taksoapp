defmodule Takso.Repo.Migrations.AddRatingToAllocation do
  use Ecto.Migration

  def change do
    alter table(:allocations) do
      add :rating, :float
    end
  end
end
