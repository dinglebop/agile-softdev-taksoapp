defmodule Takso.Repo.Migrations.AddTransportStartEndToBooking do
  use Ecto.Migration

  def change do
    alter table(:bookings) do
      add :transport_start, :utc_datetime
      add :transport_end, :utc_datetime
    end
  end
end
