# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Takso.Repo.insert!(%Takso.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Takso.{Repo, User, Taxi}

# Users
[
  # %{name: "Fred Flintstone", email: "fred.flintstone@kivi.ee", password: "paroolike", role: "customer"},
  %{name: "Barney Rubble", email: "c@a.a", password: "test1234", role: "customer"},
  # %{name: "Bilbo Baggins", email: "bilbo.baggins@lotr.me", password: "paroolike", role: "taxi-driver"},
  %{name: "Frodo Baggins", email: "d@a.a", password: "test1234", role: "taxi-driver"}
]
|> Enum.map(fn user_data -> User.changeset(%User{}, user_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

# Taxis
[
  # %{email: "bilbo.baggins@lotr.me", location: "Uus turu 2", status: "available"},
  %{email: "d@a.a", location: "Raatuse 22", status: "available"}
]
|> Enum.map(fn taxi_data -> Taxi.changeset(%Taxi{}, taxi_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)
