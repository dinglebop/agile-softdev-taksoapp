# agile-softdev-taksoapp

This repository contains the code for the project part in the course MTAT.03.295 Agile Software Development.
The project's aim was to create a taxi booking system for Strelsau, Ruritania in the form of a web application built with Elixir and Phoenix Framework on the back-end and Vue.js on the front-end.

# Installation

 - Make sure you have Postgres database available either on your local machine or over a web interface and that the file `config/dev.exs` (or `config/test.exs` in the case of testing) is pointing to it using the correct credentials.
 - Install dependencies using `mix deps.get`
 - Initialize the database using `mix ecto.setup`
 - Install front-end dependencies via `cd assets && npm i`

# Running

To run, simply run the command `mix phx.server`