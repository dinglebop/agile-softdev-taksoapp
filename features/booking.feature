Feature: Customer booking flow
  As a customer
  I want to log into my account
  And book a ride
  
  Scenario: booking on STRS' web page
    Given Credentials email "fred.flintstone@kivi.ee", password "paroolike"
    When I enter this info and press login
    And Enter a pickup address "Here and now" and dropoff address "Somewhere else"
    Then I should be able to book a ride