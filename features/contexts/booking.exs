defmodule WhiteBread.BookingContext do
  use WhiteBread.Context
  use Hound.Helpers

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end

  scenario_starting_state fn _state ->
    Hound.start_session
    navigate_to("http://localhost:4001/")
    %{}
  end

  scenario_finalize fn _status, _state ->
    Hound.end_session
  end

  given_ ~r/^Credentials email "(?<argument_one>[^"]+)", password "(?<argument_two>[^"]+)"$/,
  fn state, %{argument_one: argument_one, argument_two: argument_two} ->
    {:ok, state |> Map.put(:username, argument_one) |> Map.put(:password, argument_two)}
  end

  when_ ~r/^I enter this info and press login$/, fn state ->
    fill_field({:id, "username"}, state[:username])
    fill_field({:id, "password"}, state[:password])

    click({:id, "login"})
    {:ok, state}
  end

  and_ ~r/^Enter a pickup address "(?<argument_one>[^"]+)" and dropoff address "(?<argument_two>[^"]+)"$/,
  fn state, %{argument_one: pickup, argument_two: dropoff} ->
    fill_field({:id, "pickup"}, pickup)
    fill_field({:id, "dropoff"}, dropoff)

    click({:id, "submitbooking"})
    {:ok, state}
  end

  then_ ~r/^I should be able to book a ride$/,
  fn state ->
    {:ok, state}
  end
end
