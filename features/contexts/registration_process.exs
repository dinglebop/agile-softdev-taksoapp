defmodule WhiteBread.RegistrationProcessContext do
  use WhiteBread.Context
  use Hound.Helpers

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end

  scenario_starting_state fn _state ->
    Hound.start_session
    navigate_to("http://localhost:4001/")
    %{}
  end

  scenario_finalize fn _status, _state ->
    Hound.end_session
  end

  given_ ~r/^name "(?<argument_one>[^"]+)" and email "(?<argument_two>[^"]+)" and password "(?<argument_three>[^"]+)"$/,
  fn state, %{argument_one: argument_one, argument_two: argument_two, argument_three: argument_three} ->
    {:ok, state |> Map.put(:name, argument_one) |> Map.put(:email, argument_two) |> Map.put(:password, argument_three)}
  end

  when_ ~r/^I click on the register toolbar button$/, fn state ->
    click({:id, "toRegister"})
    {:ok, state}
  end

  and_ ~r/^I enter this info and choose customer$/, fn state ->
    fill_field({:id, "name"}, state[:name])
    fill_field({:id, "email"}, state[:email])
    fill_field({:id, "password"}, state[:password])
    click({:class, "v-select"})

    # this somehow gets the driver lol
    #find_element(:class, "v-select-list") |> click()

    # this kind of logically gets the customer
    find_element(:class, "v-list__tile__content") |> click()

    {:ok, state}
  end

  then_ ~r/^I should be able to finish registration$/, fn state ->
    click({:id, "register"})
    {:ok, state}
  end

  given_ ~r/^Credentials email "(?<argument_one>[^"]+)", password "(?<argument_two>[^"]+)"$/,
  fn state, %{argument_one: _argument_one,argument_two: _argument_two} ->
    {:ok, state}
  end

  when_ ~r/^I enter this info and press login$/,
  fn state ->
    {:ok, state}
  end

  and_ ~r/^Enter a pickup address "(?<argument_one>[^"]+)" and dropoff address "(?<argument_two>[^"]+)"$/,
  fn state, %{argument_one: _argument_one,argument_two: _argument_two} ->
    {:ok, state}
  end
end
