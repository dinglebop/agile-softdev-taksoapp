Feature: Customer registration
  As a customer
  I want to make an account
  
  Scenario: Registering via STRS' web page
    Given name "doggo man" and email "test@o.man" and password "test"
    When I click on the register toolbar button
    And I enter this info and choose customer
    Then I should be able to finish registration