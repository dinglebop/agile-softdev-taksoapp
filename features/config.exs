defmodule WhiteBreadConfig do
  use WhiteBread.SuiteConfiguration

  suite name:      "Register user",
    context:       WhiteBread.RegistrationProcessContext,
    feature_paths: ["features/registration_process.feature"]

  suite name:      "Booking",
    context:       WhiteBread.BookingContext,
    feature_paths: ["features/booking.feature"]
end
